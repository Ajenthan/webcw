<?php

class Questions extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('questionsm');
        $this->load->model('user');
    }
 

public function index()
{
    redirect('/auth/login'); // url helper function
}

public function ask(){
    $this->load->view('ask');
}

public function post(){
$title = $this->input->post('title');
$question = $this->input->post('question');
$name = $this->user->is_loggedin();
$this->questionsm->put($title, $question, $name);
redirect('auth/homeview');
}


}